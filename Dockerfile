FROM ubuntu:16.04

RUN apt update && apt install -y curl locales procps \
  && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
  && echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
  && apt update && apt install -y yarn  \
  && locale-gen en_US.UTF-8 && localedef -i en_US -f UTF-8 en_US.UTF-8

RUN curl https://raw.githubusercontent.com/euroelessar/qutim/master/src/share/qutim/ca-certs/Thawte_Primary_Root_CA.pem >> /etc/ssl/certs/ca-certificates.crt \
  && curl https://install.meteor.com | sh
